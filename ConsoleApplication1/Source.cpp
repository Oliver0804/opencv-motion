﻿#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc.hpp"
#include <opencv2/opencv.hpp>

#include <opencv2/core/ocl.hpp>
#include <opencv2/video/tracking.hpp>

#include "opencv2/video/background_segm.hpp"


#include <vector>
#include <fstream>
#include <iostream>
#include <math.h>
#include <Windows.h>

using namespace std;
using namespace cv;



int main()
{
	Mat image_1;
	Mat image_2;
	Mat GRAY_frame;

	Mat frame;
	Mat back;
	Mat fore;
	
	Mat img, foregroundMask, backgroundImage, foregroundImg;

	//cv::BackgroundSubtractorMOG2 bg;

	VideoCapture cap;      //capture的宣告
	cap.open(0);           //0 為預設camera
	//cap.set(3, 512);
	//cap.set(4, 288);
	
	cout << "成功" << endl;

	// Init background substractor
	Ptr<BackgroundSubtractor> bg_model = createBackgroundSubtractorMOG2().dynamicCast<BackgroundSubtractor>();


	//creak window
	/*
	cv::namedWindow("Frame");
	cv::namedWindow("Fore");
	cv::namedWindow("Background");
	*/

	/*
	cv::SimpleBlobDetector::Params params;
	params.minThreshold = 40;
	params.maxThreshold = 60;
	params.thresholdStep = 5;
	params.minArea = 100;
	params.minConvexity = 0.3;
	params.minInertiaRatio = 0.01;
	params.maxArea = 8000;
	params.maxConvexity = 10;
	params.filterByColor = false;
	params.filterByCircularity = false;
	cv::SimpleBlobDetector blobDtor(params);
	blobDtor.create("SimpleBlob");
	*/


	while (cap.isOpened())  //確認camera能開啓
	{
		//get image 3 way....
		cap >> image_1;
		//cap.read(image_2);
		cap.retrieve(img, CAP_OPENNI_BGR_IMAGE);

		//resize
		resize(img, img, Size(640, 480));

		if (foregroundMask.empty()) {
			cout << "create foreground mask of proper size" << endl;
			foregroundMask.create(img.size(), img.type());
		}

		// compute foreground mask 8 bit image
		// -1 is parameter that chose automatically your learning rate
		bg_model->apply(img, foregroundMask, true ? -1 : 0);


		//影像處理
		erode(image_1, fore,Mat());//侵蝕
		dilate(image_1, back, Mat());//膨脹
		//轉換灰階 image_1-> GRAY_frame
		//cvtColor(image_1, GRAY_frame, COLOR_RGB2GRAY);
		//GaussianBlur(GRAY_frame, GRAY_frame, Size(11, 11), 3.5, 3.5);//高斯模糊
		//threshold(GRAY_frame, GRAY_frame, 15, 255, THRESH_BINARY);  //閥值轉匯黑白飽和


		// create black foreground image
		foregroundImg = Scalar::all(0);
		// Copy source image to foreground image only in area with white mask
		img.copyTo(foregroundImg, GRAY_frame);

		//Get background image
		bg_model->getBackgroundImage(GRAY_frame);

		//show main webcam
		imshow("Webcam live 1", image_1);
		
		//resinz 1/2 webcam
		//resize(image_1, image_2, Size(640/2, 480/2));
		//imshow("Webcam live 2", image_2);

		imshow("foreground mask", foregroundMask);
		imshow("foreground image", foregroundImg);

		imshow("GaussianBlur", GRAY_frame);//這視窗變化比較慢

//		imshow("foregroundImg", foregroundImg);
		//imshow("fore", fore);
		//imshow("dilate", back);
		waitKey(10);//避免CPU負荷，給點delay時間
					//實際上一般webcam的framerate差不多33ms
		if (!backgroundImage.empty()) {

			imshow("mean background image", backgroundImage);
			int key5 = waitKey(40);
		}
	}

	return 0;
}
